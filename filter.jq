.resources[] |
select(.type == "vault_transit_secret_backend_key") |
.instances[] |
{index: .index_key, name: (.attributes.id | ltrimstr("transit/keys/"))} |
"terraform state mv vault_transit_secret_backend_key.key[\(.index)] 'vault_transit_secret_backend_key.key[\"\(.name)\"]'"
