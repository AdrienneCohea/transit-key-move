# Transit Key Move

Safely move Transit keys which were defined in Terraform using count instead of for_each to remove hazards due to positionality

## Getting started

The file `filter.jq` generates Terraform state move commands from a query on the state file. You can use different resource names of course to get what you want. You can also wrap ltrimstr() with rtrimstr() to remove suffixes to extract the desired named key for use in the move command.

A simple script like `move.sh` can be used to process the state file using jq and execute the generated Terraform commands using Bash.

Obviously, be careful to validate the filter, tweak it for your use case, and be sure to back up your state file beforehand so that you can restore it if something goes wrong:

```bash
terraform state pull > state-backup.json
```

To restore

```bash
terraform state push state-backup.json -force
```
